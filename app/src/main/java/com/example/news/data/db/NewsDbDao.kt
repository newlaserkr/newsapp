package com.example.news.data.db

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import com.example.news.data.models.Article

@Dao
interface NewsDbDao {
    @Insert
    suspend fun addNews(article: Article)

    /*@Insert
    suspend fun addNewsFromTopNews(articles: List<Article>)*/

    @Query("SELECT * FROM news_table ORDER BY id DESC")
    suspend fun getAllSavedNews(): List<Article>
}