package com.example.news.data.models

import com.google.gson.annotations.SerializedName

data class Source(


    /*@SerializedName("category")
    val category: String,
    @SerializedName("country")
    val country: String,
    @SerializedName("description")
    val description: String,*/

   // @ColumnInfo(name="source_id")
    @SerializedName("id")
    val id: String?,
    /*@SerializedName("language")
    val language: String,*/

    //@ColumnInfo(name="source_name")
    @SerializedName("name")
    val name: String?
  /*  @SerializedName("url")
    val url: String*/
)