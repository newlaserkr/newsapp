package com.example.news.data.models

import com.google.gson.annotations.SerializedName

data class NewsDAO(

    @SerializedName("articles")
    val articles: List<Article>,

    @SerializedName("status")
    val status: String,

    @SerializedName("totalResults")
    val totalResults: Int
)