package com.example.news.ui.sources

import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import com.example.news.R
import com.example.news.data.network.NewsApi
import com.example.news.data.repositories.NewsRepository
import kotlinx.android.synthetic.main.sources_fragment.*

class SourcesFragment : Fragment() {

    companion object {
        fun newInstance() = SourcesFragment()
    }

    private lateinit var viewModel: SourcesViewModel
    private lateinit var factory: SourcesViewModelFactory

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.sources_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        val api:NewsApi=NewsApi()
        val repository= NewsRepository(api)
        factory= SourcesViewModelFactory(repository)
        viewModel=ViewModelProviders.of(this,factory).get(SourcesViewModel::class.java)
        viewModel.getSources()
        viewModel.sources.observe(viewLifecycleOwner, Observer { sourcelist ->
            sources_recycler_view.also {
                //it.layoutManager= StaggeredGridLayoutManager(2, LinearLayoutManager.VERTICAL)
                it.layoutManager = GridLayoutManager(requireContext(),2)
                it.setHasFixedSize(true)
                it.adapter =
                    SourceRecyclerAdapter(sourcelist.sources, findNavController())
            }
        })
    }

}
